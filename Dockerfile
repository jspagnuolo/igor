FROM ubuntu:20.04
ARG DEBIAN_FRONTEND=noninteractive

SHELL ["/bin/bash", "-c"]
RUN apt-get update && apt-get install -y \
    gcc-7 \
    g++-7 \
    build-essential \
    unzip \
    python3.8 \
    python3-pip \
    wget \
    && wget https://github.com/qmarcou/IGoR/releases/download/1.4.0/igor_1-4-0.zip \
    && unzip igor_1-4-0.zip && cd igor_1-4-0 \
    && ./configure CC=gcc-7 CXX=g++-7 \
    && make clean \
    && make \
    && make install \
    && mv pygor ../pygor \
    && cd .. \
    && rm -r igor_1-4-0 \
    && rm igor_1-4-0.zip \
    && pip3 install ./pygor \
    && pip3 install pandas biopython matplotlib numpy scipy

ENV PYGOR_PATH="/pygor"    
