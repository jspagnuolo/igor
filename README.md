https://qmarcou.github.io/IGoR/
# Dependencies

    a C++ compiler supporting OpenMP 3.8 or higher and POSIX Threads (pthread) such as GCC (GNU C Compiler)

    GSL library : a subpart of the library is shipped with IGoR and will be statically linked to IGoR’s executable to avoid dependencies

    jemalloc (optional although recommended for full parallel proficiency) memory allocation library: also shipped with IGoR to avoid dependencies issues (requires a pthreads compatible compiler)

    bash

    autotools suite, asciidoctor, pygments (optional), doxygen and the latex suite if building from unpackaged sources

# Install

IGoR uses the autotools suite for compilation and installation in order to ensure portability to many systems.

Installing from packaged releases (recommended)

First download the latest released package on the Release page. Extract the files from the archive.

Installing from unpackaged sources (by cloning or direct download of the repository)

For this you will have to get git, and all other dependencies mentioned above. Note that this is the most convenient way to keep IGoR up-to-date but involves a few extra installation steps. Using git, clone the repository where you desire. Go in the created directory and run the autogen.sh bash script. This will create the configure script. Upon this stage the installation rules are the same as for packaged developer sources. From git you can chose among two branches: the master branch corresponds to the latest stable (packaged) release, the dev branch is the most up to date branch including current developpments until they are issued in the next release. The dev branch is therefore more bug prone, however this is the natural branch for people ready to help with developpment (even only by functionality testing).

A (sadly) non exhaustive list of potential installation troubleshoots follows in the next section. If your problem is not referenced there please open a GitHub issue. If you end up finding a solution by yourself please help us append it to the following list and help the user community.
	To upgrade IGoR uninstall your previously installed version and install the new one.
## Linux

Widely tested on several Debian related distros. Install gcc/g++ if not already installed (note that another compiler could be used). With the command line go to IGoR’s root directory and simply type ./configure. This will make various check on your system and create makefiles compatible with your system configuration. Many options can be appended to ./configure such as ./configure CC=gcc CXX=g+ + to enforce the use of gcc as compiler. The full set of the configure script options can be found here.

Once over, type make to compile the sources (this will take a few minutes). IGoR’s executable will appear in the igor_src folder

Finally in order to access all IGoR’s features, install IGoR by typing make install. This will install IGoR’s executable, supplied models and manual in your system’s default location (note that depending on this location you might require administrator privileges and use the sudo prefix). If you do not have administrator privileges, IGoR can be installed locally in the folder of your choice by passing --prefix=/your/custom/path upon calling the configure script (e.g ./configure --prefix=$HOME). Other configure options can be accessed using ./configure -h.

As a brief summary for default installation use the following set of commands:

```
./configure  
make  
make install  
```
	Specify your custom installation options at this step.
	Compile the sources before installation.
	Install IGoR.
	Clean uninstallation of IGoR (e.g before upgrading IGoR to a newer version) is obtained via the make uninstall command.
    
## MacOS

MacOS is shipped with another compiler (Clang) when installing Xcode that is called upon calling gcc (through name aliasing) and is not supporting OpenMP. In order to use gcc and compile with it an OpenMP application you will first need to download Macports or Homebrew and install gcc from there.

First if not already present on your system install XCode through the application store.

Macports can be found here. Download and install the version corresponding to your MacOS version.

Once installed, use Macports to install GCC:

sudo port selfupdate #Update macports database
sudo port install gcc6 #install gcc version 6

The full list of available GCC versions is available here, select a sufficiently recent one to get C++11 standards enabled. In order to set GCC as your default compiler use the following commands:

port select --list gcc #Will list the versions of gcc available on your system
sudo port select --set gcc mp-gcc6 #set the one you wish to have as default call upon using the gcc command

If you prefer to use Homebrew over Macports, it can be downloaded and installed here.

Then install GCC using the following command:

brew install gcc

Note: if you decide to use Homebrew you should apparently refrain yourself from assigning the newly installed gcc to the gcc command(see this page for more details). You will thus have to pass the correct compiler instructions to the configure script with the CC and CXX flags.

Alternatively you could also install GCC directly from sources as described by this guide.

Once done, simply follow instructions from the Linux installation section to complete IGoR’s installation.
